package dam1819.teoriuf2;

public class Alumne {

    private String nom;
    private String cognom;

    public Alumne(String nom, String cognom){
        this.nom = nom;
        this.cognom = cognom;
    }

    public String getName() {
        return this.nom;
    }

    public String getCognom() {
        return this.cognom;
    }


}
